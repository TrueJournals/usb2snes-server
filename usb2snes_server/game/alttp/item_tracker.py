from usb2snes_server.request import BaseRequest
import struct


class ItemTracker(BaseRequest):
    DUNGEONS = [
        'sewers',
        'castle-1',
        'eastern-palace',
        'desert-palace',
        'castle-2',
        'swamp-palace',
        'dark-palace',
        'misery-mire',
        'skull-woods',
        'ice-palace',
        'tower-of-hera',
        'gargoyles-domain',
        'turtle-rock',
        'ganons-tower',
    ]

    async def get(self, slot=None):
        async with self:
            rando_seed = ((await self._usb2snes.read_ram(0x7fc0, 512))[0:21]).decode('utf-8')
            if slot is None:
                sram_data = await self._usb2snes.read_wram(0xF000, 0x500)
            else:
                sram_data = await self._usb2snes.read_ram(0xe00000 + int(slot)*0x500, 0x500)
        sram_trace = sram_data[0x340:]

        rando_config = {
            'difficulty': rando_seed[15:],
            'logic': 'MajorGlitches' if rando_seed[2] == 'G' else 'OverworldGlitches' if rando_seed[2] == 'S' else 'C',
        }

        # client = tornado.httpclient.AsyncHTTPClient()
        # TODO: pass rando_config in
        # TODO: Other logic versions / things
        # rando_spoiler_r = await client.fetch("http://vt.alttp.run/seed/{}".format(rando_seed[3:12]))
        # rando_spoiler = json.loads(rando_spoiler_r.body)
        rando_spoiler = {'spoiler': {}}

        items = {
            'bow': sram_trace[0],
            'boomerang': sram_trace[1] == 1,
            'hookshot': sram_trace[2] == 1,
            'bombs': sram_trace[3],
            'mushroom': sram_trace[4],
            'fire-rod': sram_trace[5] == 1,
            'ice-rod': sram_trace[6] == 1,
            'bombos': sram_trace[7] == 1,
            'ether': sram_trace[8] == 1,
            'quake': sram_trace[9] == 1,
            'lamp': sram_trace[10] == 1,
            'hammer': sram_trace[11] == 1,
            'shovel': sram_trace[12] == 1,
            'net': sram_trace[13] == 1,
            'book': sram_trace[14] == 1,
            'cane-of-somaria': sram_trace[16] == 1,
            'cane-of-byrna': sram_trace[17] == 1,
            'cape': sram_trace[18] == 1,
            'mirror': sram_trace[19] > 0,
            'gloves': sram_trace[20],
            'boots': sram_trace[21] == 1,
            'flippers': sram_trace[22] == 1,
            'moon-pearl': sram_trace[23] == 1,
            'sword': sram_trace[25],
            'shield': sram_trace[26],
            'tunic': sram_trace[27] + 1,
            'bottle-1': sram_trace[28],
            'bottle-2': sram_trace[29],
            'bottle-3': sram_trace[30],
            'bottle-4': sram_trace[31],
            'green-pendant': (sram_trace[52] & 4) == 4,
            'blue-pendant': (sram_trace[52] & 2) == 2,
            'red-pendant': (sram_trace[52] & 1) == 1,
            'crystal-1': (sram_trace[58] & 2) == 2,
            'crystal-2': (sram_trace[58] & 16) == 16,
            'crystal-3': (sram_trace[58] & 64) == 64,
            'crystal-4': (sram_trace[58] & 32) == 32,
            'crystal-5': (sram_trace[58] & 4) == 4,
            'crystal-6': (sram_trace[58] & 1) == 1,
            'crystal-7': (sram_trace[58] & 8) == 8,
            'flute': sram_trace[12] >= 2,
            'agahnim': sram_trace[43] == 1,
            'heart-pieces': sram_trace[43] + 1,
            'health': [ sram_trace[45] / 8., sram_trace[44] / 8. ],  # current, total
            'magic': [ sram_trace[46], 0x80 ],  # current, total
            'keys': sram_trace[47],
            'magic-usage': sram_trace[59],
            
            'chests': {
                'purple-chest': (sram_trace[0x3c9 - 0x340] & (1 << 4)) == (1 << 4),
            },
            'dungeon-keys': {},
            'dungeon-deaths': {},
            'dungeon-compass': {},
            'dungeon-map': {},
            'dungeon-big-key': {},
        }

        for i in range(len(self.DUNGEONS)):
            # Keys, don't know if these work
            items['dungeon-keys'][self.DUNGEONS[i]] = sram_trace[60 + i]
            # Deaths, doesn't seem to work
            items['dungeon-deaths'][self.DUNGEONS[i]] = struct.unpack_from("<H", sram_trace, 167 + i*2)[0]

            # TODO: Not certain these are working properly yet
            dungeon_item_bit = 1 << (16 - i)
            compasses = struct.unpack_from("<H", sram_trace, 36)[0]
            items['dungeon-compass'][self.DUNGEONS[i]] = (compasses & dungeon_item_bit) > 0
            big_keys = struct.unpack_from("<H", sram_trace, 38)[0]
            items['dungeon-big-key'][self.DUNGEONS[i]] = (big_keys & dungeon_item_bit) > 0
            maps = struct.unpack_from("<H", sram_trace, 40)[0]
            items['dungeon-map'][self.DUNGEONS[i]] = (maps & dungeon_item_bit) > 0

        double_items = sram_trace[210]
        if double_items != 96:
            if double_items & 1:  # Flute working
                items['flute'] = True
            if double_items & 2:  # Flute fake
                items['flute'] = True
            if double_items & 4:
                items['shovel'] = True
            if double_items & 16:
                items['powder'] = True
            if double_items & 32:
                items['mushroom'] = True
            if double_items & 64:
                items['red-boomerang'] = True
            if double_items & 128:
                items['boomerang'] = True

        items['rupees'] = struct.unpack_from("<H", sram_trace, 34)[0]

        items['name'] = ""
        for address in range(153, 161, 2):
            character_code = struct.unpack_from("<H", sram_trace, address)[0]
            if character_code == 0x12a:
                items['name'] += 'ツ'
            elif 0x14a <= character_code <= 0x14f:
                items['name'] += chr(character_code - 0x14a + ord('A'))
            elif 0x160 <= character_code <= 0x16f:
                items['name'] += chr(character_code - 0x160 + ord('G'))
            elif 0x180 <= character_code <= 0x184:
                items['name'] += chr(character_code - 0x180 + ord('W'))
            elif character_code == 0x189:
                items['name'] += '-'
            elif character_code == 0x18c:
                items['name'] += ' '
            elif character_code == 0x18e:
                items['name'] += '~'
            else:
                items['name'] += "?({})".format(character_code)

        # ROOMS!
        items['room'] = {}
        room_data = struct.unpack_from("<296H", sram_data)
        for room_num in range(len(room_data)):
            items['room'][room_num] = {
                'chest-0': (room_data[room_num] & (1 << 4)) > 0,
                'chest-1': (room_data[room_num] & (1 << 5)) > 0,
                'chest-2': (room_data[room_num] & (1 << 6)) > 0,
                'chest-3': (room_data[room_num] & (1 << 7)) > 0,
                'chest-4': (room_data[room_num] & (1 << 8)) > 0,
                'chest-5': (room_data[room_num] & (1 << 9)) > 0,
                'key-or-item': (room_data[room_num] & (1 << 10)) > 0,
            }

        items['overworld'] = {}
        overworld_data = struct.unpack_from("<128H", sram_data, 0x280)
        for overworld_tile in range(len(overworld_data)):
            items['overworld'][overworld_tile] = {
                'item': (overworld_data[overworld_tile] & (1 << 7)) > 0,
            }

        self.write({
            'spoiler': rando_spoiler['spoiler'],
            'seed': rando_seed,
            'configuration': rando_config,
            'items': items,
        })
