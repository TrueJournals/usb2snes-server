import struct
from usb2snes_server.request import BaseRequest


class Powerup(BaseRequest):
    async def get(self):
        async with self:
            powerup_state = await self._usb2snes.read_wram(0x19, 1)

        self.write({
            'powerup_state': struct.unpack_from("B", powerup_state)[0]
        })

    async def post(self):
        powerup_state = struct.pack("B", int(self.get_query_argument('powerup_state')))
        async with self:
            await self._usb2snes.write_wram(0x19, powerup_state)

        self.write({
            'success': True
        })

class ExtraItem(BaseRequest):
    async def get(self):
        async with self:
            extra_item = await self._usb2snes.read_wram(0xDC2, 1)

        self.write({
            "extra_item": struct.unpack_from("B", extra_item)[0]
        })

    async def post(self):
        extra_item = struct.pack("B", int(self.get_query_argument("extra_item")))
        async with self:
            await self._usb2snes.write_wram(0xDC2, extra_item)

        self.write({
            "success": True
        })
