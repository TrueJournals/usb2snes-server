import tornado.web
import asyncio
import weakref
import os.path
import usb2snes
from .game import alttp
from .game import smw
from .request import *
from .websocket import Usb2SnesWebSocket


class Devices:
    def __init__(self):
        self._lock = asyncio.Lock()
        self._devices = weakref.WeakValueDictionary()
        self._ids = {}

    async def get_or_add(self, path):
        async with self._lock:
            this_device_id = self._ids.get(path, None)
            if this_device_id:
                this_device = self._devices.get(this_device_id, None)
                if this_device:
                    return this_device
                else:
                    del self._ids[path]
            new_device = usb2snes.Usb2Snes(path)
            await new_device.open()
            self._devices[id(new_device)] = new_device
            self._ids[path] = id(new_device)
            return new_device


class Usb2SnesServer(tornado.web.Application):
    def __init__(self, usb2snes, *args, **kwargs):
        self._request_args = {
            'usb2snes_': usb2snes,
            'devices': Devices(),
        }
        
        super().__init__([
            (r"/api/v1/status", Status, self._request_args),
            (r"/api/v1/files/(.*)", Files, self._request_args),
            (r"/api/v1/mkdir/(.*)", MakeDir, self._request_args),
            (r"/api/v1/boot/(.*)", BootRom, self._request_args),
            (r"/api/v1/download/(.*)", FileDownload, self._request_args),
            (r"/api/v1/patch", LivePatch, self._request_args),
            (r"/api/v1/state", StateMgmt, self._request_args),
            (r"/api/v1/ram/(?P<address>0x[0-9A-Fa-f]+)/(?P<length>[0-9]+)", RamManagement, self._request_args),
            (r"/api/v1/wram/(?P<address>0x[0-9A-Fa-f]+)/(?P<length>[0-9]+)", WramManagement, self._request_args),
            (r"/api/v1/wram/(?P<address>0x[0-9A-Fa-f]+)", WramManagement, self._request_args),
            (r"/api/v1/game/alttp/item_tracker/(?P<slot>[0-2])?", alttp.ItemTracker, self._request_args),
            (r"/api/v1/game/smw/powerup", smw.Powerup, self._request_args),
            (r"/api/v1/game/smw/extra_item", smw.ExtraItem, self._request_args),
            (r"/api/v1/websocket/", Usb2SnesWebSocket, self._request_args),
            (r"/zeldahud/(.*)", tornado.web.StaticFileHandler, { 'path': os.path.join(os.path.dirname(__file__), "../zeldahud") }),
            (r"/smw/(.*)", tornado.web.StaticFileHandler, { 'path': os.path.join(os.path.dirname(__file__), "../smw") }),
            (r"/(.*)", tornado.web.StaticFileHandler, { 'path': os.path.join(os.path.dirname(__file__), "../frontend") }),
        ], autoreload=True, websocket_ping_interval=15, websocket_ping_timeout=30, *args, **kwargs)
