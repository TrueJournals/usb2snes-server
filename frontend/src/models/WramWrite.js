"use strict";
var m = require("mithril");

import SnesWebsocket from './SnesWebsocket';

export default class WramWrite {
    // NOTE: Single-byte write ONLY
    static write(address, data) {
        address += 0x7e0000
        var cmd_part_1 = {
            addr: 0x2c00,
            data: new Uint8Array([
                // 0x08, // PHP
                0x00, // "Perform first byte last" ???
                0xE2, 0x20, // SEP #20
                0x48, // PHA
                0xEB, // XBA
                0x48, // PHA
                0xA9, data & 0xFF, // LDA #$DATA
                0x8F, address & 0xFF, (address >> 8) & 0xFF, (address >> 16) & 0xFF, // STA.l address
                0xA9, 0x00, // LDA #$00
                0x8F, 0x00, 0x2C, 0x00, // STA.l $002C00
                0x68, // PLA
                0xEB, // XBA
                0x68, // PLA
                0x28, // PLP
                0x6C, 0xEA, 0xFF, // JMP ($FFEA)
            ])
        }
        var cmd_part_2 = {
            addr: 0x2c00,
            data: new Uint8Array([
                0x08 // PHP
            ])
        }

        var ips_patch = this.makeIps([cmd_part_1, cmd_part_2])

        SnesWebsocket.send({
            "Opcode": "PutIPS",
            "Space": "CMD",
            "Flags": null,
            "Operands": ["", ips_patch.byteLength.toString(16)]
        })

        var CHUNK_SIZE = 1024
        for (var i = 0; i < ips_patch.byteLength; i += CHUNK_SIZE) {
            SnesWebsocket.websocket.send(ips_patch.slice(i, i + CHUNK_SIZE))
        }
    }

    static makeIps(parts) {
        var ips_size = "PATCH".length + parts.reduce((acc, p) => acc + p.data.byteLength + 5, 0) + "EOF".length
        var ips_patch = new Uint8Array(ips_size)

        var pos = 0
        ips_patch.set([0x50,0x41,0x54,0x43,0x48])
        pos += "PATCH".length

        parts.forEach((patch) => {
            ips_patch.set([(patch.addr >> 16) & 0xFF, (patch.addr >> 8) & 0xFF, patch.addr & 0xFF], pos)
            pos += 3

            ips_patch.set([(patch.data.byteLength >> 8) & 0xFF, patch.data.byteLength & 0xFF], pos)
            pos += 2

            ips_patch.set(patch.data, pos)
            pos += patch.data.byteLength
        })

        ips_patch.set([0x45,0x4f,0x46], pos)
        pos += "EOF".length

        return ips_patch
    }
}
