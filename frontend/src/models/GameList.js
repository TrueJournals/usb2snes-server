"use strict";
var m = require("mithril");

import ApiHelper from './ApiHelper';
import SnesWebsocket from './SnesWebsocket';
import SnesStatus from './SnesStatus';

var COLLATOR = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'})

var GameList = {
    path: null,
    directories: [],
    files: [],

    setPath: function(path) {
        this.path = path
        if (this.path[0] == "/") {
            this.path = this.path.substr(1)
        }
    },

    loadList: function(path) {
        if (path !== undefined) {
            this.setPath(path)
        }

        return SnesWebsocket.sendSingle({
            "Opcode": "List",
            "Space": "FILE",
            "Flags": null,
            "Operands": [this.path],
        }).then((data) => {
            this.directories = []
            this.files = []

            for (var i = 0; i < data.Results.length; i += 2) {
                var file = [data.Results[i], data.Results[i+1]]
                if (file[0] == 0) {
                    if (file[1] != ".") {
                        this.directories.push(file[1])
                    }
                } else {
                    this.files.push(file[1])
                }
            }

            this.directories.sort(COLLATOR.compare)
            this.files.sort(COLLATOR.compare)
        })
    },

    boot: function(rom) {
        SnesWebsocket.send({
            "Opcode": "Boot",
            "Space": "FILE",
            "Flags": null,
            "Operands": [rom],
        })
    },

    remove: function(rom) {
        SnesWebsocket.send({
            "Opcode": "Remove",
            "Space": "FILE",
            "Flags": null,
            "Operands": [rom],
        })
        this.loadList()
    },

    createDirectory: function(path) {
        SnesWebsocket.send({
            "Opcode": "MakeDir",
            "Space": "FILE",
            "Flags": null,
            "Operands": [path],
        })
        this.loadList()
    },
    
    upload: function(file_name, rom_file, progress) {
        if (typeof(progress) !== "function") {
            progress = (prg) => {}
        }

        var file_size = rom_file.size
        if (file_size === undefined) {
            file_size = rom_file.length
        }

        SnesWebsocket.send({
            "Opcode": "PutFileAck",
            "Space": "FILE",
            "Flags": null,
            "Operands": [file_name, file_size.toString(16)],
        })
        
        if (rom_file instanceof File) {
            var reader = new FileReader()
            reader.onloadend = () => {
                this.sendFileData(reader.result, progress)
            }
            reader.readAsArrayBuffer(rom_file)
        } else {
            this.sendFileData(rom_file.buffer, progress)
        }
    },
    
    sendFileData: function(arr, progress) {
        var CHUNK_SIZE = 1024

        var start = window.performance.now()

        SnesWebsocket.waitForMessageCallback().then(() => {
            SnesWebsocket.message_callback = (data) => {
                var results = JSON.parse(data)
                if (results.Done) {
                    SnesWebsocket.message_callback = null
                    var end = window.performance.now()
                    console.log(`Took ${end - start} ms to transfer ${arr.byteLength} bytes -- ${arr.byteLength/((end-start)/1000)/1024} kB/s -- chunk size ${CHUNK_SIZE}`)
                    progress(1)
                    this.loadList().then(() => { progress(null) }, () => { progress(null) })
                } else {
                    progress(results.Results[1]/arr.byteLength)
                    SnesWebsocket.websocket.send(arr.slice(results.Results[1], results.Results[1]+CHUNK_SIZE))
                }
            }

            SnesWebsocket.websocket.send(arr.slice(0, CHUNK_SIZE))
        })
    },
    
    download: function(path, file_name, progress_callback) {
        var BlobBuilder = function() {
            this.parts = []
            this.size = 0
        }
        BlobBuilder.prototype.append = function(part) {
            this.parts.push(part)
            this.size += part.size
            this.blob = undefined
        }
        BlobBuilder.prototype.getBlob = function() {
            if (!this.blob) {
                this.blob = new Blob(this.parts)
            }
            return this.blob
        }

        return new Promise((resolve, reject) => {
            var size = null
            var file_builder = new BlobBuilder()
            SnesWebsocket.waitForMessageCallback().then(() => {
                SnesWebsocket.message_callback = (data) => {
                    if (size == null) {
                        var res = JSON.parse(data)
                        size = Number.parseInt(res.Results[0], 16)
                    } else {
                        file_builder.append(data)
                        progress_callback(file_builder.size / size)

                        if (file_builder.size >= size) {
                            SnesWebsocket.message_callback = null
                            var final_blob = file_builder.getBlob()
                            resolve(URL.createObjectURL(final_blob))
                        }
                    }
                }

                SnesWebsocket.send({
                    "Opcode": "GetFile",
                    "Space": "FILE",
                    "Flags": null,
                    "Operands": [`${path}/${file_name}`],
                })
            })
        })
    }
}

module.exports = GameList
