"use strict";
var m = require("mithril");
var Stream = require("mithril/stream");

import WramArea from '../models/WramArea';

export default class ViewMemory {
    constructor(vnode) {
        this.address = new Stream(vnode.attrs.key)
        this.length = new Stream(0x200)
        if (this.address() != null) {
            this.memory = new WramArea(this.address(), this.length())
        } else {
            this.memory = null
        }

        this.interval = null
    }

    view() {
        var content = [
            m('h1', "Memory viewer"),
            m('div.form-inline', [
                m('div.form-group', [
                    m('label', [
                        "Predefined area: ",
                        m('select.form-control', { onchange: m.withAttr("value", this.setRegion.bind(this)) },
                            Object.keys(this.MEMORY_AREAS).map((area) => m('option', area))
                        )
                    ])
                ])
            ]),
            m('div.input-group.input-group-lg', [
                m('span.input-group-addon', '0x'),
                m('input.form-control#mem-addr', {type: 'text', placeholder: "Memory address", value: this.address().toString(16), oninput: m.withAttr("value", (val) => this.address(Number.parseInt(val, 16)))}),
                m('span.input-group-btn', 
                    m('button.btn.btn-primary', {
                        type: 'button',
                        onclick: this.setAddress.bind(this)
                    }, "Show")
                )
            ]),
            m('div.form-inline', [
                m('div.form-check.mb-2.mr-sm-2.mb-sm-0', [
                    m('label.form-check-label', [
                        m('input[type=checkbox]',
                            { onchange: this.setAutoUpdate.bind(this) }
                        ),
                        " Auto-refresh"
                    ])
                ])
            ]),
            m('hr')
        ]

        if (this.memory != null) {
            content.push(m('pre', this.drawMemory()))
        }
        return content
    }

    setRegion(region) {
        console.log(`${this.MEMORY_AREAS[region].address.toString(16)}  ${this.MEMORY_AREAS[region].length.toString(16)}`)
        this.address(this.MEMORY_AREAS[region].address)
        this.length(this.MEMORY_AREAS[region].length)
    }

    setAddress() {
        this.memory = new WramArea(this.address(), 512)
        this.memory.update()
    }

    oninit() {
        if (this.address() != null) {
            this.memory.update()
        }
    }

    onremove() {
        if (this.interval != null) {
            clearInterval(this.interval)
            this.interval = null
        }
    }

    drawMemory() {
        var result = "";
        var buffer = new Buffer(16);
        var i = 0;
        for (i = 0; i < this.memory.raw.length; ++i) {
            var b = this.memory.raw[i]

            if ((i % 16) == 0) {
                if (i != 0) {
                    result += `  ${buffer.toString('utf-8')}\n`
                    buffer = new Buffer(16)
                }

                result += `  ${("0000000" + i.toString(16)).slice(-8)} `
            }

            result += ` ${("00" + b.toString(16)).slice(-2)}`

            if ((b < 0x20) || (b > 0x7e)) {
                buffer[i % 16] = '.'.charCodeAt(0)
            } else {
                buffer[i % 16] = b
            }
        }

        while ((i % 16) != 0) {
            result += "   "
            ++i 
        }

        result += `  ${buffer}`

        return result + "\n"
    }

    setAutoUpdate(e) {
        if (this.interval != null) {
            clearInterval(this.interval)
            this.interval = null
        }

        if (e.target.checked) {
            this.interval = setInterval(() => this.memory.update(), 100)
        }
    }
}

ViewMemory.prototype.MEMORY_AREAS = {
    SD2SNES_RANGE: {
        address: 0xF50000,
        length: 0x200,
    },
    WRAM: {
        address: 0xF50000,
        length: 0x0020000,
    },
    VRAM: {
        address: 0xF70000,
        length: 0x0010000,
    },
    APU: {
        address: 0xF80000,
        length: 0x0010000,
    },
    CGRAM: {
        address: 0xF90000,
        length: 0x0000200,
    },
    OAM: {
        address: 0xF90200,
        length: 0x0000220,
    },
    PPUREG: {
        address: 0xF90500,
        length: 0x0000200,
    },
    CPUREG: {
        address: 0xF90700,
        length: 0x0000200,
    },
    MISC: {
        address: 0xF90420,
        length: 0x00000E0,
    },
    MSU: {
        address: 0x000000,
        length: 0x0007800,
    },
    CMD: {
        address: 0x002A00,
        length: 0x0000600,
    },
}
