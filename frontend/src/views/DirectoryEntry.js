"use strict";
var m = require("mithril");

export default class DirectoryEntry {
    constructor(vnode) {
        this.gameList = vnode.attrs.gameList
        this.name = vnode.attrs.name
    }

    view() {
        var new_path = this.name
        if (this.gameList.path != "") {
            new_path = `${this.gameList.path}/${new_path}`
        }
        if (this.name == "..") {
            var last_slash = this.gameList.path.lastIndexOf("/")
            if (last_slash == -1) {
                new_path = ""
            } else {
                new_path = new_path.substr(0, last_slash)
            }
        }
        return m(
            `a.list-group-item.list-group-item-action.list-group-item-primary[href=/list/${new_path}]`,
            { oncreate: m.route.link },
            [
                m('span.oi.oi-folder'),
                ` ${this.name}`
            ]
        )
    }
}
