"use strict";
var m = require("mithril");
var stream = require("mithril/stream");

import SnesWebsocket from '../models/SnesWebsocket';
import SnesStatus from '../models/SnesStatus';
import GameList from '../models/GameList';
import ViewStatusPage from './StatusPage';
import ViewGameList from './GameList';
import ViewMemory from './Memory';
import ZeldaHUD from './ZeldaHUD';
import Container from './Container';
import SmwCheats from './SmwCheats';

export default class ViewConnectToSnes {
    view() {
        return m('.container',
            m('form.form-attach', [
                m('h2.form-attach-heading', "Connect to SNES"),
                m('label.sr-only[for="port-select"]',
                    {
                        value: this.port(),
                        oninput: m.withAttr("value", this.port),
                    },
                    "Select Port"
                ),
                m('select.form-control[id="port-select"]',
                    SnesStatus.availablePorts.map((e) => m('option', e))
                ),
                m('button.btn.btn-lg.btn-primary.btn-block[type="submit"]',
                    {
                        disabled: SnesStatus.availablePorts.length == 0,
                        onclick: this.connect.bind(this)
                    },
                    "Connect"
                )
            ])
        )
    }

    oninit() {
        this.port = stream()
        if (SnesWebsocket.connected) {
            SnesStatus.getAvailablePorts()
        }
    }

    onupdate() {
        if (this.port() === undefined || SnesStatus.availablePorts.indexOf(this.port()) === -1) {
            if (SnesStatus.availablePorts.length > 0) {
                this.port(SnesStatus.availablePorts[0])
            } else {
                this.port(undefined)
            }
        }
    }

    connect(e) {
        e.preventDefault()
        console.log(`Asked to connect to ${this.port()}`)

        SnesWebsocket.send({
            "Opcode": "Attach",
            "Operands": [this.port()],
        })

        m.route(document.body, "/", {
            "/": {
                render: function (vnode) {
                    return m(Container, m(ViewStatusPage, vnode.attrs))
                }
            },
            "/list/:key...": {
                onmatch: args => GameList.loadList(args.key),
                render: function (vnode) {
                    return m(Container, m(ViewGameList, vnode.attrs))
                }
            },
            "/memory": {
                render: function(vnode) {
                    return m(Container, m(ViewMemory, { address: null }))
                }
            },
            "/memory/:key": {
                render: function (vnode) {
                    return m(Container, m(ViewMemory, vnode.attrs))
                }
            },
            "/zeldahud": {
                render: function(vnode) {
                    return m(Container, m(ZeldaHUD, vnode.attrs))
                }
            },
            "/smwcheats": {
                render: function(vnode) {
                    return m(Container, m(SmwCheats, vnode.attrs))
                }
            },
        })
    }
}
