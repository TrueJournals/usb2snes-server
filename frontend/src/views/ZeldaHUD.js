"use strict";
var m = require("mithril");

// import WramArea from '../models/WramArea';
import ZeldaRam from '../models/zeldahud/ZeldaRam'
import ItemDisplay from './zeldahud/ItemDisplay'

export default class ZeldaHUD {
    constructor(vnode) {
        this.data = new ZeldaRam()
    }

    view() {
        return [
            m('h1', "Zelda HUD"),
            this.data.has_loaded ? [
                m('ul', [
                    m('li', `Name: ${this.data.name()}`),
                    m('li', [
                        "Health: ",
                        this.data.health().map(x => m(`img.small-img[src=${this.getImgUrl(x)}]`)),
                    ]),
                    m('li', [
                        "Magic: ",
                        m('progress.magic', this.data.magic())
                    ]),
                    m('li', ["Rupees: ", this.data.rupees()]),
                ]),
                m('table.hud', [
                    m('tr', [
                        m(ItemDisplay, { data: this.data, key: "bow" }),
                        m(ItemDisplay, { data: this.data, key: "boomerang" }),
                        m(ItemDisplay, { data: this.data, key: "hookshot" }),
                        m(ItemDisplay, { data: this.data, key: "mushroom" }),
                        m('td', m("div.empty-item", " ")),
                        m(ItemDisplay, { data: this.data, key: "heart_pieces" }),
                        m(ItemDisplay, { data: this.data, key: "green_pendant" }),
                    ]),
                    m('tr', [
                        m(ItemDisplay, { data: this.data, key: "fire_rod" }),
                        m(ItemDisplay, { data: this.data, key: "ice_rod" }),
                        m(ItemDisplay, { data: this.data, key: "bombos" }),
                        m(ItemDisplay, { data: this.data, key: "ether" }),
                        m(ItemDisplay, { data: this.data, key: "quake" }),
                        m('td', m("div.empty-item", " ")),
                        m(ItemDisplay, { data: this.data, key: "blue_pendant" }),
                    ]),
                    m('tr', [
                        m(ItemDisplay, { data: this.data, key: "red_pendant" }),
                    ])
                ])
            ] :
            m("span", "Loading...")
        ]
    }

    oninit() {
        this.interval = setInterval(() => this.data.update(), 500)
    }

    onremove() {
        clearInterval(this.interval)
    }

    getImgUrl(id) {
        return `/img/alttp/${("000" + id).slice(-4)}.png`
    }
}
