var m = require("mithril");

import ApiHelper from '../models/ApiHelper'

export default class Menu {
    view() {
        return m('nav.navbar.navbar-expand-md.navbar-dark.bg-dark.fixed-top', [
            m('a.navbar-brand', { href: '/', oncreate: m.route.link }, "usb2snes"),
            m('button.navbar-toggler', { type: 'button', 'data-toggle': 'collapse', 'data-target': '#theNavbar', 'aria-controls': 'theNavbar', 'aria-expanded': false, 'aria-label': 'Toggle Navigation' },
                m('span.navbar-toggler-icon', '')
            ),
            m('div.collapse.navbar-collapse', { id: 'theNavbar' },
                m('ul.navbar-nav.mr-auto', [
                    m('li.nav-item',
                        m('a.nav-link', { href: '/list/', oncreate: m.route.link }, 'Games')
                    ),
                    m('li.nav-item',
                        m('a.nav-link', { href: '/memory', oncreate: m.route.link }, 'Memory')
                    ),
                    m('li.nav-item',
                        m('a.nav-link', { href: '/zeldahud', oncreate: m.route.link }, 'ZeldaHUD')
                    ),
                    m('li.nav-item',
                        m('a.nav-link', { href: '/smwcheats', oncreate: m.route.link }, 'SMW Cheats')
                    ),
                ])
            ),
            m('div.progress',
                m(`div.progress-bar.fixed-top${ApiHelper.loaded_percent === null ? ".d-none" : ApiHelper.loaded_percent < 100 ? ".progress-bar-striped.progress-bar-animated" : ""}[style='height: 5px; width: ${ApiHelper.loaded_percent}%']`, "")
            )
        ])
    }
}
