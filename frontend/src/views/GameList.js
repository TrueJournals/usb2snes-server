"use strict";
var m = require("mithril");

import GameList from '../models/GameList';
import RomPatcher from '../models/RomPatcher';
import GameEntry from './GameEntry';
import DirectoryEntry from './DirectoryEntry';

export default class ViewGameList {
    constructor(vnode) {
        this.upload_in_progress = false
        this.upload_progress = 0
        this.path = vnode.attrs.key
        if (this.path[0] == "/") {
            this.path = this.path.substr(1)
        }
    }

    view() {
        return [
            m('h1', 'File Browser'),
            m('div.row', [
                m('div.col-md-4',
                    m('ul.list-group', [
                        m('li.list-group-item',
                            m('button.btn.btn-sm.btn-success',
                                { onclick: this.makeDirectory.bind(this) },
                                [m('span.oi.oi-plus'), " New Directory"]
                            )
                        ),
                        GameList.directories.map(
                            (dir, i) => m(DirectoryEntry, { gameList: GameList, key: dir, name: dir })
                        )
                    ])
                ),
                m('div.col-md-8',
                    m('ul.list-group', [
                        m('li.list-group-item', [
                            m('button.btn.btn-sm.btn-success',
                                { onclick: this.initiateFileUpload.bind(this), disabled: this.upload_in_progress },
                                [
                                    m('span.oi.oi-data-transfer-upload'),
                                    " Upload"
                                ]
                            ),
                            " ",
                            m('progress',
                                { value: this.upload_progress, max: 100 }
                            ),
                            this.upload_in_progress ? [" ", m('span.oi.oi-reload.spinner')] : ""
                        ]),
                        GameList.files.map(
                            (file, i) => m(GameEntry, { gameList: GameList, key: file, name: file })
                        )
                    ])
                )
            ])
        ]
    }

    uploadFile(e) {
        try {
            if (e.target.files.length > 2) {
                alert("ERROR: Can only upload single ROM, or ROM + patch")
                return
            }

            if (e.target.files.length > 0) {
                this.upload_in_progress = true
                m.redraw()

                var file_name = null
                var rom_file = null
                if (e.target.files.length == 2) {
                    // Check if a patch file and ROM file are both present
                    var patch_file = null
                    for (var i = 0; i < e.target.files.length; i++) {
                        var f = e.target.files[i]
                        if (f.name.endsWith("bps")) {
                            patch_file = f
                        } else if (f.name.endsWith("smc") || f.name.endsWith("sfc")) {
                            rom_file = f
                        }
                    }

                    if (!patch_file || !rom_file) {
                        alert("ERROR: When picking two files, must pick ROM + patch")
                        throw `No patch or rom: ${rom_file}, ${patch_file}`
                    }
                    console.log(`Found patch: ${patch_file.name}; rom: ${rom_file.name}`)

                    file_name = patch_file.name.substr(0, patch_file.name.length-3) + rom_file.name.substr(rom_file.name.length-3)
                    console.log(`Final file name: ${file_name}`)

                    // Patch the ROM
                    var rom_reader = new FileReader()
                    rom_reader.onload = fe => {
                        var rom_file_data = new Uint8Array(fe.target.result)
                        var patcher = new RomPatcher(rom_file_data)

                        var patch_reader = new FileReader()
                        patch_reader.onload = pe => {
                            var patch_file_data = new Uint8Array(pe.target.result)
                            try {
                                rom_file = patcher.patch(patch_file_data)
                                GameList.upload(`${this.path}/${file_name}`, rom_file, this.updateUploadProgress.bind(this))
                            } catch(e) {
                                console.log(`Error during patch: ${e}`)
                                alert("ERROR: Could not patch game")
                                this.upload_in_progress = false
                                GameList.loadList()
                            }
                        }
                        patch_reader.readAsArrayBuffer(patch_file)
                    }
                    rom_reader.readAsArrayBuffer(rom_file)
                } else {
                    // TODO: Multiple ROMs
                    rom_file = e.target.files[0]
                    file_name = e.target.files[0].name
                    GameList.upload(`${this.path}/${file_name}`, rom_file, this.updateUploadProgress.bind(this))
                }
            }
        } catch(e) {
            console.log(`Error during upload: ${e}`)
            this.upload_in_progress = false
            GameList.loadList()
        }
    }

    updateUploadProgress(amount) {
        if (amount === null) {
            this.upload_in_progress = false
        } else {
            this.upload_progress = Math.floor(amount*100)
        }
        m.redraw()
    }

    makeDirectory() {
        var dir_name = window.prompt("New directory name", "")
        if (dir_name != null) {
            GameList.createDirectory(`${this.path}/${dir_name}`)
        }
    }

    initiateFileUpload() {
        this.upload_progress = 0
        m.redraw()
        var upload_box = document.createElement("input")
        upload_box.setAttribute("type", "file")
        upload_box.setAttribute("multiple", true)
        upload_box.setAttribute("accept", ".smc,.sfc,.bps")
        upload_box.onchange = this.uploadFile.bind(this)
        upload_box.click()
    }
}
