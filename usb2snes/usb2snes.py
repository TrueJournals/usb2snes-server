from enum import Enum, auto, Flag, IntEnum
import pprint
import struct
import serial_asyncio
import asyncio
from asyncio.locks import Lock
import io
from .ips import IPS


class CEnum(Enum):
    def _generate_next_value_(name, start, count, last_values):
        return count

class Opcode(CEnum):
    # Address space operations
    GET = auto()
    PUT = auto()
    VGET = auto()
    VPUT = auto()

    # Filesystem operations
    LS = auto()
    MKDIR = auto()
    RM = auto()
    MV = auto()

    # Special operations
    RESET = auto()
    BOOT = auto()
    POWER_CYCLE = auto()
    INFO = auto()
    MENU_RESET = auto()
    STREAM = auto()
    TIME = auto()

    # Response
    RESPONSE = auto()

class ServerSpace(CEnum):
    FILE = auto()
    SNES = auto()
    MSU = auto()
    CMD = auto()
    CONFIG = auto()

class Flags(Flag):
    NONE = 0
    SKIPRESET = 1
    ONLYRESET = 2
    CLRX = 4
    SETX = 8
    NORESP = 64
    DATA64B = 128

class FileType(IntEnum):
    DIRECTORY = 0
    FILE = 1
    END_MARKER = 2
    ERROR = 0xFF


class GetSpecifier(object):
    def __init__(self, offset: int, size: int):
        self.offset = offset
        self.size = size


class Usb2Snes(object):
    def __init__(self, device):
        self.device = device
        self.device_lock = Lock()
        self.reader = None
        self.writer = None

    def __del__(self):
        self.close()

    async def open(self):
        self.reader, self.writer = await serial_asyncio.open_serial_connection(url=self.device, baudrate=916200, timeout=5, dsrdtr=True)

    def close(self):
        if self.writer is not None:
            self.writer.close()
            self.reader = None
            self.writer = None

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        pass

    def is_connected(self):
        return self.writer is not None and self.reader is not None

    async def send_command(self, opcode, space, flags, *args):
        buffer = b""
        buf = io.BytesIO(b"\x00" * 512)
        buf.write(struct.pack("4sBBB", b"USBA", opcode.value, space.value, flags.value))
        if space == ServerSpace.FILE:
            if opcode in [Opcode.GET, Opcode.PUT, Opcode.LS, Opcode.MKDIR, Opcode.RM, Opcode.MV, Opcode.BOOT]:
                if len(args) < 1:
                    raise RuntimeError("Need arg")

                # TODO: MV
                if opcode == Opcode.PUT:
                    if len(args) < 2:
                        raise RuntimeError("Need another arg")

                    buf.seek(252)
                    buf.write(struct.pack('>I', args[1]))

                if len(args[0]) > 255:
                    args[0] = args[0][:255]

                # Arg starts at offset 256
                buf.seek(256)
                buf.write(args[0].encode())
            elif opcode not in [Opcode.RESET, Opcode.MENU_RESET, Opcode.INFO, Opcode.STREAM, Opcode.POWER_CYCLE]:
                raise RuntimeError("Invalid opcode for file command")
        else:
            if opcode in [Opcode.GET, Opcode.PUT]:
                if len(args) < 2:
                    raise RuntimeError("Need 2 args")

                # Args start at offset 252
                buf.seek(252)
                buf.write(struct.pack(">II", args[1], args[0]))
            elif opcode in [Opcode.VGET, Opcode.VPUT]:
                if len(args) == 0 or len(args) > 8:
                    raise RuntimeError("Minimum of 1, maximum of 8 args for vector instructions")

                view = buf.getbuffer()
                for i in range(len(args)):
                    view[32 + i*4] = args[i].size
                    view[33 + i*4] = (args[i].offset >> 16) & 0xFF
                    view[34 + i*4] = (args[i].offset >> 8) & 0xFF
                    view[35 + i*4] = (args[i].offset >> 0) & 0xFF
                del view

                buf.seek(0, io.SEEK_SET)
                buf.truncate(64)
            elif opcode not in [Opcode.RESET, Opcode.MENU_RESET, Opcode.INFO, Opcode.STREAM, Opcode.POWER_CYCLE]:
                raise RuntimeError("Invalid opcode")

        self.writer.write(buf.getvalue())

        if flags & Flags.NORESP:
            return

        response = await self.reader.readexactly(512)
        if len(response) < 512:
            raise RuntimeError("Unexpected packet size")
        magic, res_opcode, res_flag = struct.unpack('4sBB', response[0:6])
        if magic != b'USBA' or res_opcode != Opcode.RESPONSE.value or res_flag == 1:
            pprint.pprint(magic)
            pprint.pprint(res_opcode)
            pprint.pprint(res_flag)
            pprint.pprint(response)
            raise RuntimeError("Command error, unexpected response")

        if opcode == Opcode.LS:
            result = []
            buf = b""
            f_type = FileType.DIRECTORY
            while f_type != FileType.ERROR:
                buf = await self.reader.readexactly(512)

                pos = 0
                buffer_length = len(buf)
                while pos < buffer_length:
                    f_type = FileType(buf[pos + 0])
                    if f_type in [FileType.DIRECTORY, FileType.FILE]:
                        name_end = buf.find(b'\x00', pos+1)
                        name = buf[pos+1:name_end]
                        pos = name_end+1
                        if len(name) == 0:
                            raise RuntimeError("Probably something wrong")
                    elif f_type in [FileType.END_MARKER, FileType.ERROR]:
                        break

                    result.append((f_type, name.decode('utf-8')))
            return result
        elif opcode in [Opcode.GET, Opcode.VGET]:
            (file_size, ) = struct.unpack_from('>I', response, 252)
            return file_size
        elif opcode == Opcode.PUT:
            (file_size, ) = struct.unpack_from('>I', response, 252)
            if file_size != args[1]:
                print("WARNING: response to put has different size")
        elif opcode == Opcode.INFO:
            results = []
            start_pos = 256+4
            end_pos = response.find(b"\x00", start_pos)
            results.append(response[start_pos:end_pos].decode())

            results.append("{:x}".format(struct.unpack_from("<I", response, 256)[0]))

            start_pos = 16
            end_pos = response.find(b"\x00", start_pos)
            results.append(response[start_pos:end_pos].decode())

            raw_features = response[6]
            features = []
            if raw_features & 0x01: features.append('FEAT_DSPX')
            if raw_features & 0x02: features.append('FEAT_ST0010');
            if raw_features & 0x04: features.append('FEAT_SRTC');
            if raw_features & 0x08: features.append('FEAT_MSU1');
            if raw_features & 0x10: features.append('FEAT_213F');
            if raw_features & 0x20: features.append('FEAT_CMD_UNLOCK');
            if raw_features & 0x40: features.append('FEAT_USB1');
            if raw_features & 0x80: features.append('FEAT_DMA1');
            results.append("|".join(features))
            return results


    async def list_files(self, directory="/"):
        async with self.device_lock:
            return (await self.send_command(Opcode.LS, ServerSpace.FILE, Flags.NONE, directory))

    async def boot_rom(self, rom):
        async with self.device_lock:
            return (await self.send_command(Opcode.BOOT, ServerSpace.FILE, Flags.NONE, rom))

    async def _chunked_transfer(self, data):
        # data += b'\x00' * (512 - (len(data) % 512))
        # print(len(data))
        # self.writer.write(data)
        # await self.writer.drain()

        start_byte = 0
        size = len(data)
        while start_byte < size:
            end_byte = min(start_byte + 512, size)
            buffer = data[start_byte:end_byte]
            buffer += b'\x00' * (512-len(buffer))
            self.writer.write(buffer)
            await self.writer.drain()
            start_byte = end_byte

    async def put_file(self, path, data):
        async with self.device_lock:
            await self.send_command(Opcode.PUT, ServerSpace.FILE, Flags.NONE, path, len(data))
            await self._chunked_transfer(data)

    async def download_file(self, path: str):
        data_read = 0
        async with self.device_lock:
            file_size = (await self.send_command(Opcode.GET, ServerSpace.FILE, Flags.NONE, path))
            while data_read < file_size:
                chunk = (await self.reader.read(file_size - data_read))
                yield (file_size, chunk)
                data_read += len(chunk)

    async def delete_file(self, path):
        async with self.device_lock:
            await self.send_command(Opcode.RM, ServerSpace.FILE, Flags.NONE, path)

    async def make_directory(self, path):
        async with self.device_lock:
            await self.send_command(Opcode.MKDIR, ServerSpace.FILE, Flags.NONE, path)

    async def live_patch(self, ips_patch: IPS):
        async with self.device_lock:
            for i, patch in enumerate(ips_patch.items):
                flags = Flags.NONE
                if i == 0:
                    flags = Flags.CLRX
                elif i == (len(ips_patch.items) - 1):
                    flags = Flags.SETX

                await self.send_command(Opcode.PUT, ServerSpace.SNES, flags, patch.address, len(patch.data))
                await self._chunked_transfer(patch.data)

    async def save_state(self):
        async with self.device_lock:
            size = (await self.send_command(Opcode.GET, ServerSpace.SNES, Flags.NONE, 0xF00000, 0x60000))
            return (await self.reader.readexactly(size))

    async def restore_state(self, state):
        async with self.device_lock:
            await self.send_command(Opcode.PUT, ServerSpace.SNES, Flags.NONE, 0xF00000, len(state))
            await self._chunked_transfer(state)

    async def _read_ram(self, address, length):
        size = (await self.send_command(Opcode.GET, ServerSpace.SNES, Flags.NONE, address, length))
        if (size % 512) != 0:
            size += 512 - (size % 512)
        return (await self.reader.readexactly(size))[:length]

    async def read_ram(self, address, length):
        async with self.device_lock:
            return (await self._read_ram(address, length))

    async def _write_ram(self, address, data):
        await self.send_command(Opcode.PUT, ServerSpace.SNES, Flags.NONE, address, len(data))
        await self._chunked_transfer(data)

    async def write_ram(self, address, data):
        async with self.device_lock:
            await self._write_ram(address, data)

    async def _queued_ram_write(self, address, data):
        bytes_written = 0

        pointers = (await self._read_ram(0xF9EFF4, 4))
        send_q_head_ptr, send_q_tail_ptr = struct.unpack_from("<HH", pointers)
        send_q_head_ptr &= 0x7FC
        send_q_tail_ptr &= 0x7FC

        def next_ptr(ptr):
            ptr += 4
            if ptr >= 0x7FC:
                ptr = 0
            return ptr

        if next_ptr(send_q_tail_ptr) == send_q_head_ptr:
            raise RuntimeError("Queue already full!")

        for byte in data:
            buffer_data = address | (byte << 24)
            buffer = struct.pack("<HH", (buffer_data >> 16) & 0xFFFF, (buffer_data & 0xFFFF))
            await self._write_ram(0xF9F800 + send_q_tail_ptr, buffer)
            bytes_written += 1

            send_q_tail_ptr = next_ptr(send_q_tail_ptr)

            if send_q_tail_ptr == send_q_head_ptr:
                break

        # Advance tail pointer
        buffer = struct.pack("<H", send_q_tail_ptr)
        await self._write_ram(0xF9EFF6, buffer)

        return bytes_written

    # address is offset from 0x7E0000 -- hence wram
    async def read_wram(self, address, length):
        async with self.device_lock:
            return (await self._read_ram(address + 0xF50000, length))

    async def write_wram(self, address, data):
        async with self.device_lock:
            return (await self._queued_ram_write(address + 0x7E0000, data))

    async def info(self):
        async with self.device_lock:
            return (await self.send_command(Opcode.INFO, ServerSpace.SNES, Flags.NONE))
